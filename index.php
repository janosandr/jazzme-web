<!DOCTYPE html>
<html lang="ru">
<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <meta http-equiv="X-UA-Compatible" content="ie=edge">
    <title>Благодарить - это так просто! | JAZZME</title>

    <link href="assets/css/common.css" rel="stylesheet">
    <link rel="stylesheet" href="assets/fonts/gotham/stylesheet.css" />
    <link rel="stylesheet" href="https://unpkg.com/aos@next/dist/aos.css" />

        <link rel="apple-touch-icon" sizes="180x180" href="assets/favicon/apple-touch-icon.png">
<link rel="icon" type="image/png" sizes="32x32" href="assets/favicon/favicon-32x32.png">
<link rel="icon" type="image/png" sizes="16x16" href="assets/favicon/favicon-16x16.png">
<link rel="icon" type="image/png" sizes="192x192" href="assets/favicon/android-chrome-192x192.png">
<link rel="icon" type="image/png" sizes="256x256" href="assets/favicon/android-chrome-256x256.png">
<link rel="manifest" href="assets/favicon/site.webmanifest">
<link rel="mask-icon" href="assets/favicon/safari-pinned-tab.svg" color="#202020">
<meta name="apple-mobile-web-app-title" content="JAZZME">
<meta name="application-name" content="JAZZME">
<meta name="msapplication-TileColor" content="#202020">
<meta name="theme-color" content="#202020">
</head>
<body >
    <div class="top-banner" id="topBanner">
        <img src="assets/img/blockchainlife.png">
                        <a href="#" class="btn btn-cta">Принять участие</a>

    </div>
    <div class="nav" >
        <!-- id="navbar" -->
        <input type="checkbox" id="nav-check">
        <div class="nav-header">
          <div class="nav-title">
            <a href="/"><img src="assets/img/2x/logo.png" class="nav-logo"></a>
          </div>
        </div>
        <div class="nav-btn">
          <label for="nav-check">
            <span></span>
            <span></span>
            <span></span>
          </label>
        </div>

        <div class="nav-links">
          <a href="about.php" >О продукте</a>
          <a href="whitepaper.pdf" >White Paper</a>
          <a href="/#roadmap" >Дорожная карта</a>
          <a href="https://sale.jazzme.org/calculate.php" >Калькулятор JAZZ</a>

          <a href="/en/"> <img src="https://upload.wikimedia.org/wikipedia/commons/f/f2/Flag_of_Great_Britain_%281707%E2%80%931800%29.svg" class="nav-lang" alt="" srcset=""> </a>
          <div class="bip-course">
              <img src="assets/img/bip-logo.svg" class="bip-logo">

                <?php
        $string = file_get_contents('https://api.bip.dev/api/price');
      $json_a = json_decode($string, true);
      $price = abs($json_a["data"]["price"] / 10000);
      echo  '<span class="bip-course-value"> ' . $price . '$ </span>';
      ?>


          </div>

          </div>

          </div>
        </div>
      </div>
<div class="row">


    <div class="main">


         <div class="masthead" data-aos="fade-down" data-aos-duration="800">

                <div class="masthead-content__img">
                    <img src="assets/img/qr-profile.png"  class="masthead-img js-tilt">
                </div>
                <div class="masthead-content__text">
                    <h1 class="masthead-heading"><strong class="green">Благодарить</strong> - это так просто!</h1>
                    <p class="masthead-text">Поблагодари музыканта за то,
                        что он сделает твою жизнь немного ярче!</p>
                        <a href="#more" class="btn btn-cta">Подробнее</a>
                </div>

            </div>



        <div class="block-about" id="more" data-aos="fade-left">
            <h1 class="about-heading">Что такое <strong class="green">JAZZME</strong></h1>
            <div class="about-content">
                <div class="about-content__text">
                    <p>JazzMe - это система финансирования уличных музыкантов, художников и других уличных артистов.<br><br>Для обеспечения системы мы создадим мобильное приложение для iOS и Android и собственную цифровую монету JAZZ. </p>
                </div>

            </div>

        </div>

        <div class="block-about" data-aos="fade-right">
            <h1 class="about-heading">Собственная валюта <strong class="green">JAZZ</strong></h1>
            <div class="about-content">
                <div class="about-content__text">
                    <p>С помощью цифровой монеты <strong class="green">JAZZ</strong> внутри приложения вы сможете получать билеты на концерты звезд Российской эстрады и на различные крипто-конференции. <br><br>Вы сможете обменивать монеты на получение  скидок на музыкальные инструменты в магазинах партнерах JazzMe. Так же цифровую монету вы сможете использовать для участия в розыгрышах, отправлять ее музыкантам или просто своим друзьям.</p>
                </div>

            </div>

        </div>


        <!-- <div class="block-icons" data-aos="fade-left">
            <h1 class="icons-heading">Просто <strong class="green"> о важном</strong></h1>
            <div class="icons-content">
                <div class="icon-install">
                    <img src="assets/img/1x/ic_whitepaper.png" class="info-icon">
                    <p>Скачать White Paper</p>
                </div>
                <div class="icon-install">
                    <img src="assets/img/1x/ic_coin.png" class="info-icon">
                    <p>Экономика продукта</p>
                </div>
                <div class="icon-install">
                    <img src="assets/img/1x/ic_philosophy.png" class="info-icon">
                    <p>Философия продукта</p>
                </div>
            </div>
        </div> -->



        <div class="block-info" data-aos="fade-right">
            <div class="info-content__image" >
                <img src="assets/img/qr.png" class="qr-img js-tilt">
            </div>
            <div class="info-block__text">
                <h2 class="info-heading">Больше никаких длинных адресов</h2>
                <p class="info-text">Каждый пользователь при регистрации<br>
                    получает человеко-понятный ID,<br>
                    благодаря которому может получать платежи <br>
                    в считаные секунды</p>
                    <a href="#" class="btn btn-cta">Подробнее</a>
            </div>
        </div>



        <div class="block-about-app" data-aos="fade-left">
            <h1 class="about-app-heading">О приложении</h1>
            <p>Мы вложили весь свой опыт в то, чтобы сделать самое удобное мобильное приложение. Простые переводы, пожертвования музыкантам, список транзакций, покупка скидок, участие в конкурсах. <br>Всё это - у тебя в кармане!</p>

            <div class="tab-content">
                <div class="tab-content__image">
                    <img src="assets/img/transations.png" alt="">
                </div>
                <div class="tab-content__text">
                    <h2 class="tab-heading">Молниеносные переводы</h2>
                    <p class="tab-text">Высокую скорость транзакций обеспечивает<br>
                        блокчейн Minter. <br>
                        Скорость переводов в 200 раз выше, чем у Bitcoin,<br>
                        что гарантирует получать средства меньше чем за секунду.<br>
                        Разве не прелесть?</p>
                </div>
            </div>
            <div class="tab-content">
                <div class="tab-content__image">
                    <img src="assets/img/contest.png" alt="">
                </div>
                <div class="tab-content__text-reverse">
                    <h2 class="tab-heading">Увлекательные конкурсы</h2>
                    <p class="tab-text">Принимайте участие в розыгрышах билетов на самые лучшие мероприятия в стране<br> и рассказывайте о своём успехе друзьям</p>
                </div>
            </div>
            <div class="tab-content">
                <div class="tab-content__image">
                    <img src="assets/img/sales.png" alt="">
                </div>
                <div class="tab-content__text">
                    <h2 class="tab-heading">Выгодные скидки</h2>
                    <p class="tab-text">Получайте скидки до 100% стоимости товара в маркетплейсе,<br> не тратя при этом личные деньги
                        </p>
                </div>
            </div>
            </div>




        </div>

        <div class="block-about" id="roadmap" data-aos="fade-right">
            <h1 class="about-heading">Дорожная карта</h1>
            <ul class="timeline" id="timeline">
                <li class="li complete">
                  <div class="timestamp">
                    <span class="date">Q4 2019<span>
                  </div>
                  <div class="status">
                    <h4>Рождение идеи JM</h4>
                  </div>
                </li>
                <li class="li complete">
                  <div class="timestamp">

                    <span class="date">Q4 2019<span>
                  </div>
                  <div class="status">
                    <h4>Создание бизнес-плана</h4>
                  </div>
                </li>
                <li class="li complete">
                    <div class="timestamp">

                      <span class="date">Q4 2019<span>
                    </div>
                    <div class="status">
                      <h4>Создание сайта</h4>
                    </div>
                  </li>
                <li class="li complete">
                  <div class="timestamp">

                    <span class="date">Q1 2020<span>
                  </div>
                  <div class="status">
                    <h4>Разработка и маркетинг продукта</h4>
                  </div>
                </li>
                <li class="li">
                  <div class="timestamp">

                    <span class="date">Q1 2020<span>
                  </div>
                  <div class="status">
                    <h4>Проведение Sale раундов (Private, public, PCO)</h4>
                  </div>
                </li>
                <li class="li">
                    <div class="timestamp">

                      <span class="date">Q2 2020<span>
                    </div>
                    <div class="status">
                      <h4>Выход на зарубежный уровень</h4>
                    </div>
                  </li>
                  <li class="li">
                    <div class="timestamp">

                      <span class="date">Q2 2020<span>
                    </div>
                    <div class="status">
                      <h4>Офлайн/онлайн-маркетинг</h4>
                    </div>
                  </li>
                  <li class="li">
                    <div class="timestamp">

                      <span class="date">Q3 2020<span>
                    </div>
                    <div class="status">
                      <h4>Релиз приложения</h4>
                    </div>
                  </li>
                  <li class="li">
                    <div class="timestamp">

                      <span class="date">Q3 2020<span>
                    </div>
                    <div class="status">
                      <h4>Проведение фестиваля в свободной республике Liberland</h4>
                    </div>
                  </li>

               </ul>

        </div>

        <div class="block-about" data-aos="fade-left">
            <h1 class="about-heading">Заботимся о <strong class="green">прибыли</strong></h1>
            <div class="about-content">
                <div class="about-content__text">
                    <p>Самым прибыльным вариантом будет - делегирование монет с последующей возможностью получать пассивный доход.<br>Команда JazzMe достаточно давно на рынке криптовалют и понимает всю механику ценообразования монет! JazzMe - мы не только делаем доброе дело и помогаем уличным музыкантам, а еще и заботимся о вашей прибыли!</p>
                </div>

            </div>

        </div>
        <div class="block-integration" data-aos="fade-right">
            <h1 class="integration-heading">Интеграции</h1>
            <p>Мы интегрируем известные и удобные сервисы внутри приложения, чтобы добиться максимального удобства использования</p>
            <div class="integration-icons">
                <div class="integration-icons__item">
                    <img src="assets/img/minter.png" alt="" srcset="" class="integration-icon">
                    <a href="https://about.minter.network/Minter_White_Paper_Russian.pdf">Блокчейн</a>
                </div>
                <div class="integration-icons__item">
                    <img src="assets/img/google.png" alt="" srcset="" class="integration-icon">
                    <p>Личная информация</p>
                </div>
                <div class="integration-icons__item">
                    <img src="assets/img/firebase.png" alt="" srcset="" class="integration-icon">
                    <p>Хранение данных</p>
                </div>
            </div>
        </div>

        <div class="block-integration" data-aos="fade-left">
            <h1 class="integration-heading">Наши <strong class="green">партнёры</strong></h1>
            <div class="integration-icons">
                <div class="integration-icons__item">
                <a href="http://minterindex.net" target="_blank" rel="noopener noreferrer">
                    <img src="assets/img/index_logo.png" alt="" srcset="" class="integration-icon">
                    <p>Minter Index</p>
                </a>
            </div>
            <div class="integration-icons__item">
                <a href="https://minter.1001btc.com/ru/" target="_blank" rel="noopener noreferrer">
                    <img src="assets/img/1001_logo.png" alt="" srcset="" class="integration-icon">
                    <p>1001BTC Exchange</p>
                </a>
            </div>
            <div class="integration-icons__item">
                <a href="https://willmint.co" target="_blank" rel="noopener noreferrer">
                    <img src="assets/img/wm_logo.svg" alt="" srcset="" class="integration-icon">
                    <p>WillMint</p>
                </a>
            </div>
             <div class="integration-icons__item">
                <a href="https://tokwork.tech" target="_blank" rel="noopener noreferrer">
                    <img src="assets/img/tokenwork.png" alt="" srcset="" class="integration-icon">
                    <p>Token Work</p>
                </a>
            </div>
             <div class="integration-icons__item">
                <a href="https://blockchain-life.com/europe/ru/" target="_blank" rel="noopener noreferrer">
                    <img src="assets/img/bl_logo.png" alt="" srcset="" class="integration-icon">
                    <p>Blockchain Life</p>
                </a>
            </div>
              <div class="integration-icons__item">
                <a href="https://t.me/BipConverterBot" target="_blank" rel="noopener noreferrer">
                    <img src="assets/img/bip_converter.png" alt="" srcset="" class="integration-icon">
                    <p>Bip Converter</p>
                </a>
            </div>
            </div>
        </div>

                <div class="block-integration" data-aos="fade-right">
            <h1 class="integration-heading">Офлайн-партнёр</h1>
            <img class="image-partner" src="assets/img/blockchain_post.png">
            <div class="promo-button-block">
            <a href="https://blockchain-life.com/europe/ru/" target="_blank" class="btn btn-cta">Подробнее</a>

            </div>
        </div>

        <div class="block-team" data-aos="fade-right">
            <h1 class="team-heading">Команда</h1>
            <p>Нашу команду составляют молодые и энергичные специалисты</p>

            <div class="team">
                <div class="team-item">
                    <img src="assets/img/evgeniy.png" alt="" srcset="" class="team-photo">
                    <h3 class="team-item__name">Евгений Романов</h3 class="team-item__name">
                    <p class="team-item__vacancy">Основатель, Специалист в сфере криптовалют</p class="team-item__vacancy">
                    <a class="team-item__link" href="https://facebook.com/zhezheke">Facebook</a>
                </div>
                <div class="team-item">
                    <img src="assets/img/ivan.png" alt="" srcset="" class="team-photo">
                    <h3 class="team-item__name">Иван Андреев</h3 class="team-item__name">
                    <p class="team-item__vacancy">Проект-менеджер<br>Дизайнер</p class="team-item__vacancy">
                    <a class="team-item__link" href="http://andrv.eu">Веб-сайт</a>

                    </div>
                <div class="team-item">
                    <img src="assets/img/ilyas.png" alt="" srcset="" class="team-photo">
                    <h3 class="team-item__name">Ильяс Мамаюсупов</h3 class="team-item__name">
                    <p class="team-item__vacancy">Android-разработчик</p class="team-item__vacancy">
                        <a class="team-item__link" href="https://facebook.com/imspv">Facebook</a>
                </div>
                <div class="team-item">
                    <img src="assets/img/anton.png" alt="" srcset="" class="team-photo">
                    <h3 class="team-item__name">Антон Петряев</h3>
                    <p class="team-item__vacancy">Backend-разработчик</p>
                </div>
            </div>
            <div class="team">
                                <div class="team-item">
                    <img src="assets/img/vova.png" alt="" srcset="" class="team-photo">
                    <h3 class="team-item__name">Владимир Родинка</h3>
                    <p class="team-item__vacancy">Экономический консультант</p>
                    <a class="team-item__link" href="http://minterindex.net">MinterIndex</a>
                </div>
                                                <div class="team-item">
                    <img src="assets/img/zubkov.png" alt="" srcset="" class="team-photo">
                    <h3 class="team-item__name">Владимир Зубков</h3>
                    <p class="team-item__vacancy">Переводчик</p>
                    <a class="team-item__link" href="https://www.linkedin.com/in/vladimir-zubkov-4ab45218/">LinkedIn</a>
                </div>
            </div>
        </div>

        <div class="block-social" data-aos="fade-left">
            <h1 class="social-heading">Публикуемся здесь</h1>

            <div class="social">
                <div class="social__item">
                    <a href="http://instagram.com/jazzqr" target="_blank" rel="noopener noreferrer" class="btn btn-muted btn-instagram">Instagram</a>
                </div>

                <div class="social__item">
                    <a href="https://cryptotalk.org/topic/17006-%D0%BF%D0%BE%D0%B6%D0%B5%D1%80%D1%82%D0%B2%D0%BE%D0%B2%D0%B0%D0%BD%D0%B8%D1%8F-%D0%B4%D0%BB%D1%8F-%D1%83%D0%BB%D0%B8%D1%87%D0%BD%D1%8B%D1%85-%D0%BC%D1%83%D0%B7%D1%8B%D0%BA%D0%B0%D0%BD%D1%82%D0%BE%D0%B2/?tab=comments#comment-331113" target="_blank" rel="noopener noreferrer" class="btn btn-muted btn-regular">CryptoTalk</a>
                </div>
                <div class="social__item">
                    <a href="tg://resolve?domain=jazzqr" target="_blank" rel="noopener noreferrer" class="btn btn-muted btn-telegram">Telegram</a>
                </div>
                <div class="social__item">
                    <a href="https://www.youtube.com/channel/UCQwAiLv30AShXhLq4bpxYnQ/videos?view_as=subscriber" target="_blank" rel="noopener noreferrer" class="btn btn-muted btn-youtube">YouTube</a>
                </div>
                <div class="social__item">
                    <a href="https://twitter.com/JazzMe60398179" target="_blank" rel="noopener noreferrer" class="btn btn-muted btn-twitter">Twitter</a>
                </div>
            </div>
        </div>

    </div>



</div>
<footer class="footer">
    <div class="footer-logo">
        <img src="assets/img/2x/logo_mini.png" alt="" srcset="" class="logo">
    </div>
    <div class="footer-content">

        <div class="footer-column">
            <div class="footer__item">
                <a href="whitepaper.pdf">White Paper</a>
            </div>
            <div class="footer__item">
                <a href="/#roadmap">Дорожная карта</a>
            </div>
            <div class="footer__item">
                <a href="about.html">О продукте</a>
            </div>
            <!-- <div class="footer__item">
                <a href="#">Скачать приложение</a>
            </div> -->
        </div>
        <div class="footer-column">

            <div class="footer__item">
                <a href="tg://resolve?domain=jazzqr">Telegram</a>
            </div>
            <div class="footer__item">
                <a href="https://cryptotalk.org/topic/17006-%D0%BF%D0%BE%D0%B6%D0%B5%D1%80%D1%82%D0%B2%D0%BE%D0%B2%D0%B0%D0%BD%D0%B8%D1%8F-%D0%B4%D0%BB%D1%8F-%D1%83%D0%BB%D0%B8%D1%87%D0%BD%D1%8B%D1%85-%D0%BC%D1%83%D0%B7%D1%8B%D0%BA%D0%B0%D0%BD%D1%82%D0%BE%D0%B2/?tab=comments#comment-331113">CryptoTalk</a>
            </div>

            <div class="footer__item">
                <a href="https://vk.com/jazzqr">VK</a>
            </div>
            <div class="footer__item">
                <a href="https://www.youtube.com/channel/UCQwAiLv30AShXhLq4bpxYnQ/videos?view_as=subscriber">YouTube</a>
            </div>
            <div class="footer__item">
                <a href="https://twitter.com/JazzMe60398179">Twitter</a>
            </div>
            <div class="footer__item">
                <a href="https://instagram.com/jazzqr">Instagram</a>
            </div>
        </div>
        <div class="footer-column">

            <div class="footer__item">
                <span class="footer-label">Почта</span>
                <a href="mailto://info@jazzme.org">info@jazzme.org</a>
            </div>
            <div class="footer__item">
                <span class="footer-label">Связь с основателем</span>
                <a href="tg://resolve?domain=fireth">@fireth</a>
            </div>
        </div>
    </div>
</footer>

<script src="https://code.jquery.com/jquery-3.3.1.js" type="text/javascript"></script>
<script src="assets/js/tilt.jquery.js" type="text/javascript"></script>
<script src="https://unpkg.com/aos@next/dist/aos.js"></script>
  <script>
    AOS.init();
  </script>

  <!-- Global site tag (gtag.js) - Google Analytics -->
<script async src="https://www.googletagmanager.com/gtag/js?id=UA-154496670-2"></script>
<script>
  window.dataLayer = window.dataLayer || [];
  function gtag(){dataLayer.push(arguments);}
  gtag('js', new Date());

  gtag('config', 'UA-154496670-2');
</script>


<script src="assets/js/app.js" type="text/javascript"></script>
</body>
</html>
<!DOCTYPE html>
<html lang="ru">
<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <meta http-equiv="X-UA-Compatible" content="ie=edge">
    <title>Showing appreciate made simple! | JAZZME</title>

    <link href="/assets/css/common.css" rel="stylesheet">
    <link rel="stylesheet" href="/assets/fonts/gotham/stylesheet.css" />
    <link rel="stylesheet" href="https://unpkg.com/aos@next/dist/aos.css" />

        <link rel="apple-touch-icon" sizes="180x180" href="/assets/favicon/apple-touch-icon.png">
<link rel="icon" type="image/png" sizes="32x32" href="/assets/favicon/favicon-32x32.png">
<link rel="icon" type="image/png" sizes="16x16" href="/assets/favicon/favicon-16x16.png">
<link rel="icon" type="image/png" sizes="192x192" href="/assets/favicon/android-chrome-192x192.png">
<link rel="icon" type="image/png" sizes="256x256" href="/assets/favicon/android-chrome-256x256.png">
<link rel="manifest" href="/assets/favicon/site.webmanifest">
<link rel="mask-icon" href="/assets/favicon/safari-pinned-tab.svg" color="#202020">
<meta name="apple-mobile-web-app-title" content="JAZZME">
<meta name="application-name" content="JAZZME">
<meta name="msapplication-TileColor" content="#202020">
<meta name="theme-color" content="#202020">
</head>
<body >
    <div class="nav" id="navbar">
        <input type="checkbox" id="nav-check">
        <div class="nav-header">
          <div class="nav-title">
            <a href="/"><img src="/assets/img/2x/logo.png" class="nav-logo"></a>
          </div>
        </div>
        <div class="nav-btn">
          <label for="nav-check">
            <span></span>
            <span></span>
            <span></span>
          </label>
        </div>

        <div class="nav-links">
          <a href="about.php" >About</a>
          <a href="whitepaper.pdf" >White Paper</a>
          <a href="/#roadmap" >Roadmap</a>
          <a href="https://sale.jazzme.org/calculate.php" >Calculator JAZZ</a>

          <a href="/"> <img src="https://upload.wikimedia.org/wikipedia/en/thumb/f/f3/Flag_of_Russia.svg/1200px-Flag_of_Russia.svg.png" class="nav-lang" alt="" srcset=""> </a>
          <div class="bip-course">
              <img src="/assets/img/bip-logo.svg" class="bip-logo">

                <?php
        $string = file_get_contents('https://api.bip.dev/api/price');
      $json_a = json_decode($string, true);
      $price = abs($json_a["data"]["price"] / 10000);
      echo  '<span class="bip-course-value"> ' . $price . '$ </span>';
      ?>


          </div>

          </div>

          </div>
        </div>
      </div>
<div class="row">


    <div class="main">


         <div class="masthead" data-aos="fade-down" data-aos-duration="800">

                <div class="masthead-content__img">
                    <img src="/assets/img/qr-profile.png"  class="masthead-img js-tilt">
                </div>
                <div class="masthead-content__text">
                    <h1 class="masthead-heading">Showing <strong class="green">appreciation</strong> made simple!</h1>
                    <p class="masthead-text">Appreciate the works of musicians who enriched your life!</p>
                        <a href="#more" class="btn btn-cta">Learn more</a>
                </div>

            </div>



        <div class="block-about" id="more" data-aos="fade-left">
            <h1 class="about-heading">What is <strong class="green">JAZZME</strong></h1>
            <div class="about-content">
                <div class="about-content__text">
                    <p>JazzMe is a platform designed to fund street musicians, artists and other street art makers.
<br><br>The system will consist of a mobile application for both iOS and Android, as well as our own digital JAZZ coin.
</p>
                </div>

            </div>

        </div>

        <div class="block-about" data-aos="fade-right">
            <h1 class="about-heading">Our <strong class="green">JAZZ</strong> currency</h1>
            <div class="about-content">
                <div class="about-content__text">
                    <p>Using the digital <strong class="green">JAZZ</strong> coin within the application you are able to obtain tickets to Russian pop-star concerts and various crypto conferences. <br><br>You can exchange the coins for discounts on musical instruments in JazzMe partner stores. You can also use the coins to participate in prize draws and send them as gifts to your favourite musicians or friends.</p>
                </div>

            </div>

        </div>



        <div class="block-info" data-aos="fade-right">
            <div class="info-content__image" >
                <img src="/assets/img/qr.png" class="qr-img js-tilt">
            </div>
            <div class="info-block__text">
                <h2 class="info-heading">No more lenghty addresses</h2>
                <p class="info-text">During the registration each user receives<br> a human-friendly ID, which allows to receive payments in seconds<br>

                    <a href="#" class="btn btn-cta">Learn more</a>
            </div>
        </div>



        <div class="block-about-app" data-aos="fade-left">
            <h1 class="about-app-heading">About our app</h1>
            <p>We have put our best efforts and expertise to create the most user friendly mobile app.
Easy transfers, donations to musicians, list of transactions, discounts, participating in prize draws.
<br>All of this - in your pocket!</p>

            <div class="tab-content">
                <div class="tab-content__image">
                    <img src="/assets/img/transations.png" alt="">
                </div>
                <div class="tab-content__text">
                    <h2 class="tab-heading">Lightning fast transfers</h2>
                    <p class="tab-text">Rapid transaction speed is provided by the Minter blockchain.<br>
Transfer speeds are 200 times faster than the Bitcoin blockchain, which guarantees the receipt of funds in less than a second.<br>
How cool is this?</p>
                </div>
            </div>
            <div class="tab-content">
                <div class="tab-content__image">
                    <img src="/assets/img/contest.png" alt="">
                </div>
                <div class="tab-content__text-reverse">
                    <h2 class="tab-heading">Exciting prize draws</h2>
                    <p class="tab-text">Take part in ticket draws for the best events in the country and tell your friends all about your success
</p>
                </div>
            </div>
            <div class="tab-content">
                <div class="tab-content__image">
                    <img src="/assets/img/sales.png" alt="">
                </div>
                <div class="tab-content__text">
                    <h2 class="tab-heading">Useful discounts</h2>
                    <p class="tab-text">Receive discounts up to 100% of the cost of goods in the marketplace,<br>
without needing to spend any personal money
                        </p>
                </div>
            </div>
            </div>




        </div>

        <div class="block-about" id="roadmap" data-aos="fade-right">
            <h1 class="about-heading">Roadmap</h1>
            <ul class="timeline" id="timeline">
                <li class="li complete">
                  <div class="timestamp">
                    <span class="date">Q4 2019<span>
                  </div>
                  <div class="status">
                    <h4>JM idea was born</h4>
                  </div>
                </li>
                <li class="li complete">
                  <div class="timestamp">

                    <span class="date">Q4 2019<span>
                  </div>
                  <div class="status">
                    <h4>Business plan creation</h4>
                  </div>
                </li>
                <li class="li complete">
                    <div class="timestamp">

                      <span class="date">Q4 2019<span>
                    </div>
                    <div class="status">
                      <h4>Website creation</h4>
                    </div>
                  </li>
                <li class="li">
                  <div class="timestamp">

                    <span class="date">Q1 2020<span>
                  </div>
                  <div class="status">
                    <h4>Product Development and Marketing</h4>
                  </div>
                </li>
                <li class="li">
                  <div class="timestamp">

                    <span class="date">Q1 2020<span>
                  </div>
                  <div class="status">
                    <h4>Sale rounds (Private, Public, PCO)</h4>
                  </div>
                </li>
                <li class="li">
                    <div class="timestamp">

                      <span class="date">Q2 2020<span>
                    </div>
                    <div class="status">
                      <h4>Entering the global market</h4>
                    </div>
                  </li>
                  <li class="li">
                    <div class="timestamp">

                      <span class="date">Q2 2020<span>
                    </div>
                    <div class="status">
                      <h4>Offline/Online marketing</h4>
                    </div>
                  </li>
                  <li class="li">
                    <div class="timestamp">

                      <span class="date">Q3 2020<span>
                    </div>
                    <div class="status">
                      <h4>App Relise</h4>
                    </div>
                  </li>
                  <li class="li">
                    <div class="timestamp">

                      <span class="date">Q3 2020<span>
                    </div>
                    <div class="status">
                      <h4>Festival hosted in independent republic of Liberland</h4>
                    </div>
                  </li>

               </ul>

        </div>

        <div class="block-about" data-aos="fade-left">
            <h1 class="about-heading">We care about <strong class="green">your profits</strong></h1>
            <div class="about-content">
                <div class="about-content__text">
                    <p>The most profitable option is going to be achieved by delegating coins with the subsequent opportunity to receive passive income.
The JazzMe team has long been in the cryptocurrency market and fully understands the mechanics of coin pricing!
JazzMe - we are not only doing a good deed helping street musicians, but also taking care of your profits!</p>
                </div>

            </div>

        </div>
        <div class="block-integration" data-aos="fade-right">
            <h1 class="integration-heading">Integrations</h1>
            <p>We integrate well-known and convenient services inside the application to achieve maximum usability</p>
            <div class="integration-icons">
                <div class="integration-icons__item">
                    <img src="/assets/img/minter.png" alt="" srcset="" class="integration-icon">
                    <a href="https://minter.network/">Blockchain</a>
                </div>
                <div class="integration-icons__item">
                    <img src="/assets/img/google.png" alt="" srcset="" class="integration-icon">
                    <p>Personal information</p>
                </div>
                <div class="integration-icons__item">
                    <img src="/assets/img/firebase.png" alt="" srcset="" class="integration-icon">
                    <p>Data storage</p>
                </div>
            </div>
        </div>

        <div class="block-integration" data-aos="fade-left">
            <h1 class="integration-heading">Our <strong class="green">partners</strong></h1>
            <div class="integration-icons">
                <div class="integration-icons__item">
                <a href="http://minterindex.net" target="_blank" rel="noopener noreferrer">
                    <img src="/assets/img/index_logo.png" alt="" srcset="" class="integration-icon">
                    <p>Minter Index</p>
                </a>
            </div>
            <div class="integration-icons__item">
                <a href="https://minter.1001btc.com/ru/" target="_blank" rel="noopener noreferrer">
                    <img src="/assets/img/1001_logo.png" alt="" srcset="" class="integration-icon">
                    <p>1001BTC Exchange</p>
                </a>
            </div>
            <div class="integration-icons__item">
                <a href="https://willmint.co" target="_blank" rel="noopener noreferrer">
                    <img src="/assets/img/wm_logo.svg" alt="" srcset="" class="integration-icon">
                    <p>WillMint</p>
                </a>
            </div>
             <div class="integration-icons__item">
                <a href="https://tokwork.tech" target="_blank" rel="noopener noreferrer">
                    <img src="/assets/img/tokenwork.png" alt="" srcset="" class="integration-icon">
                    <p>Token Work</p>
                </a>
            </div>
            <div class="integration-icons__item">
                <a href="https://blockchain-life.com/" target="_blank" rel="noopener noreferrer">
                    <img src="/assets/img/bl_logo.png" alt="" srcset="" class="integration-icon">
                    <p>Blockchain Life</p>
                </a>
            </div>
                          <div class="integration-icons__item">
                <a href="https://t.me/BipConverterBot" target="_blank" rel="noopener noreferrer">
                    <img src="/assets/img/bip_converter.png" alt="" srcset="" class="integration-icon">
                    <p>Bip Converter</p>
                </a>
            </div>
            </div>
        </div>



        <div class="block-team" data-aos="fade-left">
            <h1 class="integration-heading">Our team</h1>
            <p>Our team consists of talented and energetic specialists</p>

            <div class="team">
                <div class="team-item">
                    <img src="/assets/img/evgeniy.png" alt="" srcset="" class="team-photo">
                    <h3 class="team-item__name">Eugene Romanov</h3 class="team-item__name">
                    <p class="team-item__vacancy">Founder, <br>Crypto specialist</p class="team-item__vacancy">
                    <a class="team-item__link" href="https://facebook.com/zhezheke">Facebook</a>
                </div>
                <div class="team-item">
                    <img src="/assets/img/ivan.png" alt="" srcset="" class="team-photo">
                    <h3 class="team-item__name">Ivan Andreiev</h3 class="team-item__name">
                    <p class="team-item__vacancy">Project Manager, <br>Designer</p class="team-item__vacancy">
                    <a class="team-item__link" href="http://andrv.eu">Веб-сайт</a>

                    </div>
                <div class="team-item">
                    <img src="/assets/img/ilyas.png" alt="" srcset="" class="team-photo">
                    <h3 class="team-item__name">Ilyas Mamayusupov</h3 class="team-item__name">
                    <p class="team-item__vacancy">Android-developer</p class="team-item__vacancy">
                        <a class="team-item__link" href="https://facebook.com/imspv">Facebook</a>

                </div>
                <div class="team-item">
                    <img src="/assets/img/anton.png" alt="" srcset="" class="team-photo">
                    <h3 class="team-item__name">Anthony Petryaev</h3>
                    <p class="team-item__vacancy">Backend-разработчик</p>
                </div>
                                <div class="team-item">
                    <img src="/assets/img/vova.png" alt="" srcset="" class="team-photo">
                    <h3 class="team-item__name">Vladimir Rodinka</h3>
                    <p class="team-item__vacancy">Finance consultant</p>
                    <a class="team-item__link" href="http://minterindex.net">MinterIndex</a>
                </div>
            </div>
        </div>

        <div class="block-social" data-aos="fade-right">
            <h1 class="social-heading">Follow us on
</h1>

            <div class="social">
                <div class="social__item">
                    <a href="http://instagram.com/jazzqr" target="_blank" rel="noopener noreferrer" class="btn btn-muted btn-instagram">Instagram</a>
                </div>

                <div class="social__item">
                    <a href="https://cryptotalk.org/topic/17006-%D0%BF%D0%BE%D0%B6%D0%B5%D1%80%D1%82%D0%B2%D0%BE%D0%B2%D0%B0%D0%BD%D0%B8%D1%8F-%D0%B4%D0%BB%D1%8F-%D1%83%D0%BB%D0%B8%D1%87%D0%BD%D1%8B%D1%85-%D0%BC%D1%83%D0%B7%D1%8B%D0%BA%D0%B0%D0%BD%D1%82%D0%BE%D0%B2/?tab=comments#comment-331113" target="_blank" rel="noopener noreferrer" class="btn btn-muted btn-regular">CryptoTalk</a>
                </div>
                <div class="social__item">
                    <a href="tg://resolve?domain=jazzqr" target="_blank" rel="noopener noreferrer" class="btn btn-muted btn-telegram">Telegram</a>
                </div>
                <div class="social__item">
                    <a href="https://www.youtube.com/channel/UCQwAiLv30AShXhLq4bpxYnQ/videos?view_as=subscriber" target="_blank" rel="noopener noreferrer" class="btn btn-muted btn-youtube">YouTube</a>
                </div>
                <div class="social__item">
                    <a href="https://twitter.com/JazzMe60398179" target="_blank" rel="noopener noreferrer" class="btn btn-muted btn-twitter">Twitter</a>
                </div>
            </div>
        </div>

    </div>



</div>
<footer class="footer">
    <div class="footer-logo">
        <img src="/assets/img/2x/logo_mini.png" alt="" srcset="" class="logo">
    </div>
    <div class="footer-content">

        <div class="footer-column">
            <div class="footer__item">
                <a href="whitepaper.pdf">White Paper</a>
            </div>
            <div class="footer__item">
                <a href="/#roadmap">Roadmap</a>
            </div>
            <div class="footer__item">
                <a href="about.html">About</a>
            </div>
            <!-- <div class="footer__item">
                <a href="#">Скачать приложение</a>
            </div> -->
        </div>
        <div class="footer-column">

            <div class="footer__item">
                <a href="tg://resolve?domain=jazzqr">Telegram</a>
            </div>
            <div class="footer__item">
                <a href="https://cryptotalk.org/topic/17006-%D0%BF%D0%BE%D0%B6%D0%B5%D1%80%D1%82%D0%B2%D0%BE%D0%B2%D0%B0%D0%BD%D0%B8%D1%8F-%D0%B4%D0%BB%D1%8F-%D1%83%D0%BB%D0%B8%D1%87%D0%BD%D1%8B%D1%85-%D0%BC%D1%83%D0%B7%D1%8B%D0%BA%D0%B0%D0%BD%D1%82%D0%BE%D0%B2/?tab=comments#comment-331113">CryptoTalk</a>
            </div>

            <div class="footer__item">
                <a href="https://vk.com/jazzqr">VK</a>
            </div>
            <div class="footer__item">
                <a href="https://www.youtube.com/channel/UCQwAiLv30AShXhLq4bpxYnQ/videos?view_as=subscriber">YouTube</a>
            </div>
            <div class="footer__item">
                <a href="https://twitter.com/JazzMe60398179">Twitter</a>
            </div>
            <div class="footer__item">
                <a href="https://instagram.com/jazzqr">Instagram</a>
            </div>
        </div>
        <div class="footer-column">

            <div class="footer__item">
                <span class="footer-label">E-Mail</span>
                <a href="mailto://info@jazzme.org">info@jazzme.org</a>
            </div>
            <div class="footer__item">
                <span class="footer-label">CEO</span>
                <a href="tg://resolve?domain=fireth">@fireth</a>
            </div>
        </div>
    </div>
</footer>

<script src="https://code.jquery.com/jquery-3.3.1.js" type="text/javascript"></script>
<script src="/assets/js/tilt.jquery.js" type="text/javascript"></script>
<script src="https://unpkg.com/aos@next/dist/aos.js"></script>
  <script>
    AOS.init();
  </script>

  <!-- Global site tag (gtag.js) - Google Analytics -->
<script async src="https://www.googletagmanager.com/gtag/js?id=UA-154496670-2"></script>
<script>
  window.dataLayer = window.dataLayer || [];
  function gtag(){dataLayer.push(arguments);}
  gtag('js', new Date());

  gtag('config', 'UA-154496670-2');
</script>


<script src="/assets/js/app.js" type="text/javascript"></script>
</body>
</html>
<!DOCTYPE html>
<html lang="ru">
<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <meta http-equiv="X-UA-Compatible" content="ie=edge">
    <title>О продукте | JAZZME</title>

    <link href="assets/css/common.css" rel="stylesheet">
    <link rel="stylesheet" href="assets/fonts/gotham/stylesheet.css" />
    <link rel="stylesheet" href="https://unpkg.com/aos@next/dist/aos.css" />

        <link rel="apple-touch-icon" sizes="180x180" href="assets/favicon/apple-touch-icon.png">
<link rel="icon" type="image/png" sizes="32x32" href="assets/favicon/favicon-32x32.png">
<link rel="icon" type="image/png" sizes="16x16" href="assets/favicon/favicon-16x16.png">
<link rel="icon" type="image/png" sizes="192x192" href="assets/favicon/android-chrome-192x192.png">
<link rel="icon" type="image/png" sizes="256x256" href="assets/favicon/android-chrome-256x256.png">
<link rel="manifest" href="assets/favicon/site.webmanifest">
<link rel="mask-icon" href="assets/favicon/safari-pinned-tab.svg" color="#202020">
<meta name="apple-mobile-web-app-title" content="JAZZME">
<meta name="application-name" content="JAZZME">
<meta name="msapplication-TileColor" content="#202020">
<meta name="theme-color" content="#202020">
</head>
<body >
    <div class="nav" id="navbar">
        <input type="checkbox" id="nav-check">
        <div class="nav-header">
          <div class="nav-title">
            <a href="/"><img src="assets/img/2x/logo.png" class="nav-logo"></a>
          </div>
        </div>
        <div class="nav-btn">
          <label for="nav-check">
            <span></span>
            <span></span>
            <span></span>
          </label>
        </div>

        <div class="nav-links">
            <a href="about.html" >О продукте</a>
            <a href="whitepaper.pdf" >White Paper</a>
          <a href="https://sale.jazzme.org/calculate.php" >Калькулятор JAZZ</a>
            <a href="/#roadmap" >Дорожная карта</a>
            <a href="/en/"> <img src="https://upload.wikimedia.org/wikipedia/commons/f/f2/Flag_of_Great_Britain_%281707%E2%80%931800%29.svg" class="nav-lang" alt="" srcset=""> </a>
                      <div class="bip-course">
              <img src="assets/img/bip-logo.svg" class="bip-logo">

                <?php
        $string = file_get_contents('https://api.bip.dev/api/price');
      $json_a = json_decode($string, true);
      $price = abs($json_a["data"]["price"] / 10000);
      echo  '<span class="bip-course-value"> ' . $price . '$ </span>';
      ?>


          </div>

            </div>
        </div>
      </div>
<div class="row">


    <div class="block-inner">

<h1>Что такое JazzMe</h1>
<p><b>JazzMe</b> - это система финансирования уличных музыкантов, художников и других артистов. Мы разрабатываем мобильное приложение на iOS и Android. Приложение будет удобно в пользовании и просто в функционале. В приложении будет доступно множество функций обмена Jazz монетой, переводы друзьям, музыкантам, художникам, бонусные монеты при регистрации на первые действия, маркетплейс, рейтинговая система видео выступлений музыкантов, система лайков, что позволит выводить видео выступления в топы, тем самым повышая популярность артиста! Музыканты и обычные пользователи сети Jazzme смогут хранить монеты, обменивать их на скидки в магазинах на музыкальные инструменты и сувениры, менять их на основную монету BIP в блокчейне Minter, на эфир биткоин и другие криптовалюты, участвовать в розыгрышах билетов на различные крипто конференции например Blockchain Russia и на большие концерты музыкальных групп. К примеру пользователь сможет накопить 1000 монет у купить гитару или кофту/кепку/футболку с 100% скидкой! Скажем гитара стоит 10 000 JAZZ и музыкант сможет купить ее за накопленные 10000 JAZZ приобретя, приминив и показав электронный купон продавцу.

    <br><br>Мы придерживаемся снг политике. Так же у нас хорошие отношения и планы со свободным государством <a href="https://ru.wikipedia.org/wiki/%D0%9B%D0%B8%D0%B1%D0%B5%D1%80%D0%BB%D0%B5%D0%BD%D0%B4">Liberland</a> Jazzme выступит в качестве партнера на музыкальном фестивале Floating Man в августе 2020 года! Проект <a href="https://t.me/joinchat/AAAAAFawXo6JyGC_2Z80bg">JazzMe</a> первым на блокчейне <a href="https://www.minter.network/ru">Minter</a> будет утверждаться на соблюдение  <a href="http://andrv.eu/media/cryptorules_rus.pdf">кодекса этики</a> в использования данных на территории <a href="https://t.me/cryptopravo/1759 ">Российской Федерации!</a>

    <br><br>На данном этапе (дорожная карта) мы уже можем анонсировать наше приложение, с последующими систематическими обновлениями. Приложение на данный момент разрабатывается (что делается) у нас есть 43 страницы обсуждений на стороннем <a href="https://cryptotalk.org/topic/17006-%D1%81%D0%B8%D1%81%D1%82%D0%B5%D0%BC%D0%B0-%D1%84%D0%B8%D0%BD%D0%B0%D0%BD%D1%81%D0%B8%D1%80%D0%BE%D0%B2%D0%B0%D0%BD%D0%B8%D1%8F-%D1%83%D0%BB%D0%B8%D1%87%D0%BD%D1%8B%D1%85-%D0%BC%D1%83%D0%B7%D1%8B%D0%BA%D0%B0%D0%BD%D1%82%D0%BE%D0%B2-%D0%B8-%D0%B0%D1%80%D1%82%D0%B8%D1%81%D1%82%D0%BE%D0%B2/">форуме</a> тема имеет более 1000 комментариев и более 10.000 просмотров! Мы разработали айдентику и сейчас будем заниматься брэндингом компании JM! Планы на 2020 год - расширение, выпуск монеты JAZZ. Монету JAZZ можно будет обменять на любую другую кастомную монету в сети минтера, на основную монету сети BIP, биток, эфир и любую другую криптовалюту. Все просто и практически в один клик :)

    <br><br>Мы будем использовать Google Authentication в качестве метода аутентификации! При регистрации в приложении пользователи будут получать 50 монет в качестве бонуса на первые действия! Аутентификация через Google authentication решит проблему мультиаккунтов и обеспечит безопасность вашего кошелька в приложении должным способом! Плюс мы предусмотрели момент двойной авторизации через Google аутентификатор по временному коду. Но скорее всего, это добавим в обновлениях безопасности, между релизом и вторым апдейтом.

    <br><br>Как уже было отмечено основные планы на 2020 год - расширение не только JM и Minter, а популяризация самой концепции - IoT (Intertnet of Money) поэтому мы будем расширяться не только в снг зоне, а будем выходить на мир! Территориально мы находимся в России. В Петербурге. Команда интернациональная. Двое разработчиков с Казани, проект менеджер с Киева. Комьюнити в 430 человек на канале и 200 человек в чате (ссылка) Со временем мы переведем идею Jazzme на более чем 45 языков! (начнем с ближайшей европы) присоединяйтесь сейчас пока мы в самом начале. Датой рождения JAZZME можно считать 29 октября 2019 года. За 1.5 месяца мы собрали команду, провели аналитику бизнес плана, продумали экономику, политику и философию продукта! Со временем будем расширятся и принимать решения вместе! Мы наращиваем функционал вокруг ядра программы. Ядром выступают такие функции, как: кошелёк пользователя, безопасные пополнение и перевод денег. А вокруг него добавим конкурсы, маркетплейс и другие развлекательные фишки, барабан удачи, подарки сувениров, конкурсы, розыгрыши и прослушивание любимых песен! Ближе к релизу наберём команду тестеров, которая будет искать недочёты за приятные бонусы, чтобы делать продукт вместе ещё круче!


    <br><br>О прибыли для инвесторов. Для того что бы выпустить монету в сети минтер необходим просчет экономики, определение с количеством выпуска монет, ее эмиссии, стоимости, CRR и резервный фонд в монете BIP.
    </p>


    <h1>Что мы предлагаем</h1>
    <p>
    Мы анонсируем набор первых партнеров и открываем приват сейл <b>PRIVATE-IMO (Initial Model Offering) JAZZZME</b><br><br>

    <strong class="green">SOFTCAP: 1 000 000 BIP <br>

    HARDCAP: 10 000 000 BIP</strong>

    <br><br>Минимальный резервный фонд - 1.000.000 BIP. Мы ищем 10 партнеров для создания резервного фонда. Минимальная сумма для входа в управляющий совет проекта - 125.000 BIP, максимальная - 1.000.000 BIP. Максимально количество участников в управляющем совете - 10.

    <br><br>После собранной суммы мы объявим IMO-Public Sale, а после анонсируем вечный <a href="https://about.minter.network/Minter_PCO_Russian.pdf">PCO (Perpetual Coin Ofering)</a>

    </p>

    <h1>Что будет в дальше?</h1>
    <p>
    После создания резервного фонда, мы создадим и выпустим монету JAZZ в течении одной недели после сбора средств на резерв. Ранниt инвесторы с приват раунда получат, не только доступ к своим вложенным для резервного фонда средствам, а еще и бонус в размере 1 000 000 JAZZ (100 000 BIP) Общее количество монет будет - 10 000 000 JAZZ (при достижении soft-cap, при достижении хард капа монет будет больше)
    </p>
    <h1>Как будет расти цена монеты JAZZ? </h1>
    <p>Очень просто. Что-бы цена росла, нужно что бы монету покупали. Мы ограничили вход до 10 человек на первом этапе сбора средств на резервный фонд. После создания монеты будет объявлен паблик раунд и отрыт вход мелким инвесторам. После паблик раунда мы анонсируем PCO (вечное предложение монет) для новых пользователей Minter и сторонников концепции IoM. Наша целевая аудитория это участники сообщества Minter, Defi, Liberland, крипто-энтузиасты, уличные музыканты и просто любители послушать музыку. Это мы к тому что риск спекуляций минимален. Все участники всех сейл раундов будут оповещены об экономической модели проекта JazzMe. Хранители нашей монеты будут получать пассивную прибыль от делегирования монет JAZZ. Монета будет использоваться в внутри сети, переводы в сети будут доступны через @username и через QR-штрих код внутри приложения!

    <br><br>Монетой будут пользоваться, монета НЕ будет торговаться на спекулятивных биржах, она будет обмениваться внутри самой экосистемы минтер! От нее никто не будет стараться избавиться или продать, ее только будут покупать, что бы пользоваться ей, обменивать ее на скидки, розыгрыши и билеты на концерты звезд российской эстрады и различные криптоконференции, что, тем самым будет провоцировать рост монеты JAZZ!

    <br><br>Все крипто сообщества постепенно развиваются! Minter творит историю.  Осталось только придумать для монеты применение. Мы уже придумали. JazzMe. Minter.</p></div>
</div>
<footer class="footer">
    <div class="footer-logo">
        <img src="assets/img/2x/logo_mini.png" alt="" srcset="" class="logo">
    </div>
    <div class="footer-content">

        <div class="footer-column">
            <div class="footer__item">
                <a href="whitepaper.pdf">White Paper</a>
            </div>
            <div class="footer__item">
                <a href="roadmap.html">Дорожная карта</a>
            </div>
            <div class="footer__item">
                <a href="about.html">О продукте</a>
            </div>
            <!-- <div class="footer__item">
                <a href="#">Скачать приложение</a>
            </div> -->
        </div>
        <div class="footer-column">

            <div class="footer__item">
                <a href="tg://resolve?domain=jazzqr">Telegram</a>
            </div>
            <div class="footer__item">
                <a href="https://cryptotalk.org/topic/17006-%D0%BF%D0%BE%D0%B6%D0%B5%D1%80%D1%82%D0%B2%D0%BE%D0%B2%D0%B0%D0%BD%D0%B8%D1%8F-%D0%B4%D0%BB%D1%8F-%D1%83%D0%BB%D0%B8%D1%87%D0%BD%D1%8B%D1%85-%D0%BC%D1%83%D0%B7%D1%8B%D0%BA%D0%B0%D0%BD%D1%82%D0%BE%D0%B2/?tab=comments#comment-331113">CryptoTalk</a>
            </div>

            <div class="footer__item">
                <a href="https://vk.com/jazzqr">VK</a>
            </div>
            <div class="footer__item">
                <a href="https://www.youtube.com/channel/UCQwAiLv30AShXhLq4bpxYnQ/videos?view_as=subscriber">YouTube</a>
            </div>
            <div class="footer__item">
                <a href="https://twitter.com/JazzMe60398179">Twitter</a>
            </div>
            <div class="footer__item">
                <a href="https://instagram.com/jazzqr">Instagram</a>
            </div>
        </div>
        <div class="footer-column">

            <div class="footer__item">
                <span class="footer-label">Почта</span>
                <a href="mailto://info@jazzme.org">info@jazzme.org</a>
            </div>
            <div class="footer__item">
                <span class="footer-label">Связь с основателем</span>
                <a href="tg://resolve?domain=fireth">@fireth</a>
            </div>
        </div>
    </div>
</footer>

<script src="https://code.jquery.com/jquery-3.3.1.js" type="text/javascript"></script>
<script src="assets/js/tilt.jquery.js" type="text/javascript"></script>
<script src="https://unpkg.com/aos@next/dist/aos.js"></script>
  <script>
    AOS.init();
  </script>

  <!-- Global site tag (gtag.js) - Google Analytics -->
<script async src="https://www.googletagmanager.com/gtag/js?id=UA-154496670-2"></script>
<script>
  window.dataLayer = window.dataLayer || [];
  function gtag(){dataLayer.push(arguments);}
  gtag('js', new Date());

  gtag('config', 'UA-154496670-2');
</script>


<script src="assets/js/app.js" type="text/javascript"></script>
</body>
</html>